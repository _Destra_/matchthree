////////////////////////////////////////////////////////////////////////////////
// Copyright Antonin "Destra" Gagelin  
////////////////////////////////////////////////////////////////////////////////

package com.destra.matchthree
{
	import com.destra.tileworld.AssetsManager;
	import com.destra.toolbox.loader.ILoader;
	import com.destra.toolbox.path.Uri;
	import starling.display.Sprite;
	
	/**
	 * @author Antonin 'Destra' Gagelin (destravul[AT]gmail[DOT]com)
	 * Jan 30, 2014
	 */
	public class Game extends Sprite
	{
		////////////////////////////////////////////////////////////
		//::// Constructor 
		////////////////////////////////////////////////////////////
		
		public function Game()
		{
			AssetsManager.init();
			AssetsManager.onAssetLoaded.add(assetLoadedHandler);
			AssetsManager.loadAsset(new Uri("../Assets/SpriteSheet/spritesheet.ss"));
		}
		
		////////////////////////////////////////////////////////////
		//::// Events handlers 
		////////////////////////////////////////////////////////////
		
		private function assetLoadedHandler(pLoader : ILoader) : void
		{
			trace("Sprite sheet loaded");
		}
	}
}
