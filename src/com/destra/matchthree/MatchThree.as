////////////////////////////////////////////////////////////////////////////////
// Copyright Antonin "Destra" Gagelin  
////////////////////////////////////////////////////////////////////////////////

package com.destra.matchthree
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import starling.core.Starling;
	
	/**
	 * @author Antonin 'Destra' Gagelin (destravul[AT]gmail[DOT]com)
	 * Jan 30, 2014
	 */
	[SWF(width = "640", height = "640", frameRate = "60", backgroundColor = "#FFFFFF", wmode = "direct")]
	public class MatchThree extends Sprite
	{
		////////////////////////////////////////////////////////////
		//::// Members 
		////////////////////////////////////////////////////////////
		
		private var _starling : Starling;
		
		////////////////////////////////////////////////////////////
		//::// Constructor 
		////////////////////////////////////////////////////////////
		
		public function MatchThree()
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			_starling = new Starling(Game, stage);
			_starling.start();
			_starling.showStats = true;
		}
	}
}
