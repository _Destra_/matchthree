////////////////////////////////////////////////////////////////////////////////
// Copyright Antonin "Destra" Gagelin  
////////////////////////////////////////////////////////////////////////////////

package com.destra.matchthree.enum
{
	
	/**
	 * @author Antonin 'Destra' Gagelin (destravul[AT]gmail[DOT]com)
	 * Jan 30, 2014
	 */
	public class ColorTokenEnum
	{
		////////////////////////////////////////////////////////////
		//::// Static members / Constants 
		////////////////////////////////////////////////////////////
		
		public static const BLUE : String = "blue";
		public static const GREEN : String = "green";
		public static const BLACK : String = "black";
		public static const RED : String = "red";
		public static const YELLOW : String = "yellow";
		public static const PURPLE : String = "purple";
		public static const NEUTRAL : String = "neutral";
		public static const BONUS : String = "bonus";
	}
}
