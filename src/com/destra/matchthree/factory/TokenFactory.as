////////////////////////////////////////////////////////////////////////////////
// Copyright Antonin "Destra" Gagelin  
////////////////////////////////////////////////////////////////////////////////

package com.destra.matchthree.factory
{
	import com.destra.matchthree.display.Token;
	import com.destra.toolbox.pool.Pool;
	
	/**
	 * @author Antonin 'Destra' Gagelin (destravul[AT]gmail[DOT]com)
	 * Jan 30, 2014
	 */
	public class TokenFactory
	{
		////////////////////////////////////////////////////////////
		//::// Static members / Constants 
		////////////////////////////////////////////////////////////
		
		private static var _tokenPool : Pool;
		
		////////////////////////////////////////////////////////////
		//::// Static methods 
		////////////////////////////////////////////////////////////
		
		/**
		 * @see com.destra.enum.ColorTokenEnum
		 */
		public static function getToken(pColor : String) : Token
		{
			var token : Token = _tokenPool.getObject() as Token;
			
			return token;
		}
		
		/**
		 * Init the factory.
		 */
		public static function init() : void
		{
			_tokenPool = new Pool(Token, 10, -1, 10);
		}
	}
}
